﻿using System;
using System.Windows.Forms;

namespace ClipSave
{
    /// <summary>
    /// クリップボード保存用プログラム
    /// </summary>
    class Program
    {
        [STAThread]
        static void Main( string[] args )
        {
            // クリップボードを取得
            var cb = Clipboard.GetDataObject();

            // 画像データならイメージダイアログを表示
            using( var fm = new ImagePreview( cb ) )
            {
                if( fm.ImageData != null )
                {
                    fm.ShowDialog();
                }
            }

            // テキストデータならテキストダイアログを表示
            using( var fm2 = new TextPreview( cb ) )
            {
                if( fm2.TextData.Length > 0 )
                {
                    fm2.ShowDialog();
                }
            }
        }
    }
}
