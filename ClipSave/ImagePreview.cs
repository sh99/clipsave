﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace ClipSave
{
    ///**********************************************************************************
    /// <summary>
    /// 
    /// </summary>
    public partial class ImagePreview : Form
    {
        //---
        /// <summary>
        /// 
        /// </summary>
        public Image ImageData { get; private set; } = null;

        //===
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="cb"></param>
        public ImagePreview( IDataObject cb )
        {
            InitializeComponent();

            // イメージデータを取得
            if( cb.GetDataPresent( DataFormats.Bitmap ) )
            {
                ImageData = (Image)(cb.GetData( DataFormats.Bitmap ));
            }
        }

        //===
        /// <summary>
        /// クリップデータを保存します
        /// </summary>
        /// <param name="filename"></param>
        public bool SaveAs( string filename )
        {
            var ret = (ImageData != null);
            var file = Path.ChangeExtension( filename, "png" );

            ImageData?.Save( file, ImageFormat.Png );

            return ret;
        }

        //---
        /// <summary>
        /// フォームロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImagePreview_Load( object sender, EventArgs e )
        {
            if( ImageData != null )
            {
                pictureBox1.Image = ImageData;
            }
        }

        //---
        /// <summary>
        /// 保存ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_SaveAs_Click( object sender, EventArgs e )
        {
            var name = $"$_{DateTime.Now.ToString( "yyMMdd_HHmmssff" )}";    // ベース名

            // 保存
            if( SaveAs( name ) )
            {
                MessageBox.Show( this, $"イメージを保存しました\n[{name}]", "クリップボード保存", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
            else
            {
                MessageBox.Show( this, $"保存失敗しました。\n[{name}]", "クリップボード保存", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }

            // フォームクローズ
            this.Close();
        }

        //---
        /// <summary>
        /// 印刷ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Print_Click( object sender, EventArgs e )
        {
            // TODO: 未実装
            throw new NotImplementedException();
        }
    }
}
