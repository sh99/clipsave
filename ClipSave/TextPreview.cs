﻿using System;
using System.IO;
using System.Windows.Forms;

namespace ClipSave
{
    public partial class TextPreview : Form
    {
        //---
        /// <summary>
        /// テキストデータ
        /// </summary>
        public string TextData { get; private set; } = string.Empty;

        //==========================================================================
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="cb"></param>
        public TextPreview( IDataObject cb )
        {
            InitializeComponent();

            if( cb.GetDataPresent( DataFormats.Text ) )
            {
                TextData = (string)cb.GetData( DataFormats.Text );
            }
        }

		//==========================================================================
		/// <summary>
		/// 保存処理
		/// </summary>
		public bool SaveAs( string bn )
        {
            var ret = false;

            if( TextData.Length > 0 )
            {
                var file = Path.ChangeExtension( bn, "txt" );

                File.WriteAllText( file, TextData );
                ret = true;
            }

            return ret;
        }

		//==========================================================================
		/// <summary>
		/// 保存ボタン
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void button_SaveAs_Click( object sender, EventArgs e )
        {
            var name = $"$_{DateTime.Now.ToString( "yyMMdd_HHmmssff" )}";    // ベース名

            // 保存
            if( SaveAs( name ) )
            {
                MessageBox.Show( this, $"保存しました\n[{name}]", "クリップボード保存", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
            else
            {
                MessageBox.Show( this, $"保存失敗しました。\n[{name}]", "クリップボード保存", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }

            // フォームクローズ
            this.Close();
        }

		//==========================================================================
		/// <summary>
		/// フォームロード
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void TextPreview_Load( object sender, EventArgs e )
        {
			textBox1.Text = TextData;
			textBox1.SelectionStart = 0;
        }

		//==========================================================================
		/// <summary>
		/// 印刷ボタン
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void button_Print_Click( object sender, EventArgs e )
        {
            new NotImplementedException();
        }
    }
}
