﻿namespace ClipSave
{
    partial class TextPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextPreview));
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.button_SaveAs = new System.Windows.Forms.ToolStripButton();
			this.button_Print = new System.Windows.Forms.ToolStripButton();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.textBox1.Location = new System.Drawing.Point(1, 26);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(607, 267);
			this.textBox1.TabIndex = 0;
			this.textBox1.WordWrap = false;
			// 
			// toolStrip1
			// 
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.button_SaveAs,
            this.button_Print});
			this.toolStrip1.Location = new System.Drawing.Point(1, 1);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Padding = new System.Windows.Forms.Padding(3, 0, 1, 0);
			this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
			this.toolStrip1.Size = new System.Drawing.Size(607, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// button_SaveAs
			// 
			this.button_SaveAs.Image = ((System.Drawing.Image)(resources.GetObject("button_SaveAs.Image")));
			this.button_SaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.button_SaveAs.Name = "button_SaveAs";
			this.button_SaveAs.Size = new System.Drawing.Size(69, 22);
			this.button_SaveAs.Text = "保存(&S)";
			this.button_SaveAs.Click += new System.EventHandler(this.button_SaveAs_Click);
			// 
			// button_Print
			// 
			this.button_Print.Image = ((System.Drawing.Image)(resources.GetObject("button_Print.Image")));
			this.button_Print.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.button_Print.Name = "button_Print";
			this.button_Print.Size = new System.Drawing.Size(69, 22);
			this.button_Print.Text = "印刷(&P)";
			this.button_Print.Click += new System.EventHandler(this.button_Print_Click);
			// 
			// TextPreview
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(609, 294);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.toolStrip1);
			this.MinimumSize = new System.Drawing.Size(300, 300);
			this.Name = "TextPreview";
			this.Padding = new System.Windows.Forms.Padding(1);
			this.Text = "TextPreview";
			this.Load += new System.EventHandler(this.TextPreview_Load);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton button_SaveAs;
        private System.Windows.Forms.ToolStripButton button_Print;
    }
}